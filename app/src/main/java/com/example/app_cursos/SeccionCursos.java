package com.example.app_cursos;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class SeccionCursos extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seccion_cursos);
    }
    public void ContentCurso(View view){
        Intent info = new Intent(this, ContentCurso.class);
        startActivity(info);
    }
}