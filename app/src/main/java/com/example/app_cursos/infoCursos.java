package com.example.app_cursos;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class infoCursos extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_cursos);
    }
    public void seccioncursos(View view){
        Intent info = new Intent(this, SeccionCursos.class);
        startActivity(info);
    }

}