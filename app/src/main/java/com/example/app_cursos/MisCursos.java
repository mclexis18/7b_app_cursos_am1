package com.example.app_cursos;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MisCursos extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mis_cursos);
    }
    public void infoCursos2(View view){
        Intent info = new Intent(this, infoCursos.class);
        startActivity(info);
    }
}